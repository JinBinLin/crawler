package crawler;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.Connection.Response;
import org.jsoup.nodes.Document;

import com.spreada.utils.chinese.ZHConverter;

/**
 * Servlet implementation class wiki
 */
@WebServlet("/wiki")
public class wiki extends HttpServlet {
	private static boolean isStart = false;
	private int count = 0;
	private int countMax = 0;

	public wiki() {
		super();
	}

	public void init(ServletConfig config) throws ServletException {
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");// 設定網頁丟出需求時，用UTF-8編碼
		response.setCharacterEncoding("UTF-8");// 列出訊息時，用UTF-8編碼
		if (isStart) {
			response.getWriter().append("is run:" + count + "/" + countMax);
		} else {
			if (request.getParameter("tableName") != null) {
				if (request.getParameter("wordtag") != null) {
					control(request.getParameter("tableName"), request.getParameter("wordtag"), null);
					response.getWriter().append("ok");
				} else if (request.getParameter("keyword") != null) {
					control(request.getParameter("tableName"), null, request.getParameter("keyword"));
					response.getWriter().append("ok");
				} else {
					control(request.getParameter("tableName"), null, null);
					response.getWriter().append("ok");
				}
			} else {
				response.getWriter().append("請輸入資料表名稱");
			}
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

	private String getdata(String searchName, boolean isagain) {
		try {
			Response object = Jsoup.connect(
					"https://zh-tw.wikipedia.org/w/api.php?format=xml&action=query&prop=extracts&redirects=true&exintro&explaintext&utf8&titles="
							+ URLEncoder.encode(searchName, "UTF-8"))
					.header("User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64; Trident/7.0; rv:11.0) like Gecko")
					.header("content-encoding", "gzip, deflate").header("Accept-Language", "zh-TW").execute();
			String content = object.parse().select("extract").text();
			if (!isagain && content.equalsIgnoreCase("")) {
				return getdata(ZHConverter.convert(searchName, ZHConverter.SIMPLIFIED), true);
				// System.out.println(ZHConverter.convert(searchName, ZHConverter.SIMPLIFIED));
			} else {
				return content;
			}
		} catch (Exception e) {
			System.out.println(e.toString());
			return "";
		}
	}

	private void control(final String tableName, final String tagName, final String keyword) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				if (isStart)
					return;
				isStart = true;
				readDB DB = new readDB("learn");
				if (tableName == null)
					return;
				String DBSql = "SELECT `keyword` FROM " + tableName;
				if (tagName != null)
					DBSql = "SELECT `keyword` FROM " + tableName + " WHERE `wordtag` = '" + tagName + "'";
				if (keyword != null) {
					DBSql = "SELECT `keyword` FROM " + tableName + " WHERE `keyword` = '" + keyword + "'";
				}
				ArrayList<HashMap<String, String>> dataList = DB.getDBresult(DBSql);
				countMax = dataList.size();
				for (HashMap<String, String> item : dataList) {
					String content = getdata(item.get("keyword"), false);
					count++;
					System.out.println(content);
					DB.writeDB("UPDATE " + tableName + " SET abstract = '" + content + "' where `keyword` = '"
							+ item.get("keyword") + "'");
				}
				DB.closeDB();
				DB = null;
				count = 0;
				countMax = 0;
				isStart = false;
			}
		}).start();

	}

}
