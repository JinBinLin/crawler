package crawler;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

/**
 * Servlet implementation class news
 */
@WebServlet(urlPatterns= {"/news"},loadOnStartup = 1)
public class news extends HttpServlet {
	private final String tableName = "ts_news";// 資料庫名稱
	private final String dateColumnName = "publishdate";// 資料庫發行日期欄位名稱
	private final String titleColumnName = "title";// 資料庫標題欄位名稱
	private final String keywordColumnName = "keyword";// 資料庫關鍵字欄位名稱
	private final String authorColumnName = "author";// 資料庫作者欄位名稱
	private final String imgurlColumnName = "imgurl";// 資料庫圖片連結欄位名稱
	private final String contentColumnName = "	content";// 資料庫內容欄位名稱
	private final String urlColumnName = "url";// 資料庫新聞連結位置欄位名稱
	private final String sourseColumnName = "sourse";// 資料庫來源欄位名稱
	private final String categoryColumnName = "category";// 資料庫分類欄位名稱
	
	private int time = 60 * 60 * 24 ;// 設定週期(s)
	private Timer timer = new Timer();
	private ArrayList<String> error = new ArrayList<>();
	private int count = 0;
	private ArrayList<Integer> runTime = new ArrayList();
	
	public void init(ServletConfig config) throws ServletException {
		timer.schedule(new TimerTask() {
			@Override
			public void run() {
				udn();//魔鏡爬蟲function
			}
		}, 0, time * 1000);
	}	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		if (request.getParameter("key") != null && request.getParameter("key").equalsIgnoreCase(setData.key)) {// 檢驗key
			if (request.getParameter("time") != null) {// 設定週期
				try {
					time = Integer.parseInt(request.getParameter("time"));
					timer.cancel();// 關掉鬧鐘
					timer = new Timer();// 開啟鬧鐘
					timer.schedule(new TimerTask() {// 設定鬧鐘時間
						@Override
						public void run() {
							udn();//魔鏡爬蟲function
						}
					}, time * 1000, time * 1000);
					response.getWriter().println("setTime " + time + "s");
				} catch (Exception e) {
					response.getWriter().println("setTime error" + e.toString());
				}
			} else if(request.getParameter("resetCount") != null){
				count = 0;
				response.getWriter().println("resetCount:" + count);
			} else if(request.getParameter("resetError") != null){
				error.clear();
				response.getWriter().println("resetError:" + error.size());
			}else { 
				int day = time / (60 * 60 * 24);
				int hour = (time / (60 * 60)) % 24;
				int minute = (time / 60) % 60;
				int second = time % 60;
				response.getWriter().print(
						"新聞爬蟲運行次數:" + count + 
						"</br>新聞爬蟲運行週期:" + day + "天" + hour + "時" + minute + "分" + second + "秒"+
						"</br>新聞爬蟲上一次運行時間:" + runTime.get(0) + "年"+ runTime.get(1) + "月"+ runTime.get(2) + "日" + runTime.get(3) + "時" + runTime.get(4) + "分" + runTime.get(5) + "秒"+
						"</br>錯誤次數:"+error.size());
				if(error.size()>0) {
					for(String message:error)
					response.getWriter().print("</br>錯誤訊息:"+message);
				}
			}
		}
	}
	//聯合新聞網
	private void udn() {
//		System.out.println("news start");
		count++;
		readDB DB = new readDB();
		runTime.clear();
		runTime.add(Calendar.getInstance().get(Calendar.YEAR));
		runTime.add(Calendar.getInstance().get(Calendar.MONTH)+1);
		runTime.add(Calendar.getInstance().get(Calendar.DAY_OF_MONTH));
		runTime.add(Calendar.getInstance().get(Calendar.HOUR_OF_DAY));
		runTime.add(Calendar.getInstance().get(Calendar.MINUTE));
		runTime.add(Calendar.getInstance().get(Calendar.SECOND));
		try {
		Document document = Jsoup.connect("https://udn.com/rssfeed/lists/2").get();
		Elements temp = document.select(".group dt a");
		for (int x = 0; x < temp.size(); x++) {
			if (temp.get(x).attr("href").indexOf("https://udn.com") == 0) 
			{
				document = Jsoup.connect(temp.get(x).attr("href")).get();
				Elements urlele = document.select("item link");
				for (int j = 0; j < urlele.size(); j++) {
					String url = urlele.get(j).text();
					
					Document doc = Jsoup.connect(url).get();
					Elements ele = doc.select("#mainbar #story_body_content p");
					
					String classname = doc.select("#mainbar #nav a").get(1).text();
					
					if (classname.equals("要聞"))
						{ classname = "政治"; }
					String title = doc.select("#mainbar .story_art_title").text();
					
					String date = doc.select("#mainbar .story_bady_info_author span").text();
					String author = doc.select("#mainbar .story_bady_info_author").text().replaceAll(date, "");
					String source = "聯合新聞網";
					
					Elements imgtemp = doc.select("#mainbar #story_body_content img");
					String imgurl = new String();
					for (int i = 0; i < imgtemp.size(); i++) {
						imgurl += imgtemp.get(i).attr("src");
					}
					
					String content = new String();
					for (int i = 0; i < ele.size(); i++) {
						content += ele.get(i).text();
					}
					
					Elements keywords = doc.select("#mainbar #story_tags a");
					String keyword = new String();
					for (int i = 0; i < keywords.size(); i++) {
						keyword += keywords.get(i).text() + "|";
					}
					// 檢查資料是否重複 並且 寫入資料庫
					ArrayList<HashMap<String, String>> dbItem = DB.getDBresult("Select * from "+tableName + " where "+titleColumnName +"='"+title.replaceAll("'", "’")+"' and "+dateColumnName +"='" + date+"'");
					if(dbItem.size()==0) {// 沒有重複資料
						DB.writeDB("insert into " + tableName + "(" + 
								titleColumnName + "," + dateColumnName	+ "," + keywordColumnName + "," + authorColumnName+ "," + 
								imgurlColumnName+ "," + contentColumnName	+ "," + urlColumnName+ "," + sourseColumnName+ "," + 
								categoryColumnName+ ") values('" + 
								title.replaceAll("'", "’")+ "','" + date + "','" + keyword+ "','" + author.replaceAll("'", "’")+ "','" + imgurl+ "','" + 
								content.replaceAll("'", "’")+ "','" + url+ "','" + source+ "','" + classname+"')");
					}
//					System.out.println("news");
				
				}
			}
		}
		}catch (Exception e) {
			error.add(e.toString());
		}
		DB.closeDB();
		DB = null;
	}

}
