package crawler;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.invoke.MethodHandles;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import javafx.scene.control.ListView;

/**
 * Servlet implementation class drama
 */
@WebServlet(urlPatterns = "/drama" ,loadOnStartup = 1)
public class drama extends HttpServlet {
	private final String tableName = "st_drama";// 資料庫名稱
	private final String titleColumnName = "dr_title";// 戲劇名稱欄位名稱
	private final String areaColumnName = "dr_area";// 戲劇地區欄位名稱
	private final String yearColumnName = "dr_year";// 戲劇年分欄位名稱
	private final String partColumnName = "dr_part";// 戲劇欄位名稱
	private final String actorColumnName = "dr_actor";// 戲劇演員欄位名稱
	private final String infoColumnName = "dr_info";// 戲劇資訊欄位名稱
	private int time = 60 * 60 * 24 * 7;// 設定週期(s)
	private Timer timer = new Timer();
	private ArrayList<String> error = new ArrayList<>();
	private int count = 0;
	private ArrayList<Integer> litvRunTime = new ArrayList();
	
	public void init(ServletConfig config) throws ServletException {
		timer.schedule(new TimerTask() {
			@Override
			public void run() {
				litv();//爬蟲function
			}
		}, 0, time * 1000);
	}	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		if (request.getParameter("key") != null && request.getParameter("key").equalsIgnoreCase(setData.key)) {// 檢驗key
			if (request.getParameter("time") != null) {// 設定週期
				try {
					time = Integer.parseInt(request.getParameter("time"));
					timer.cancel();// 關掉鬧鐘
					timer = new Timer();// 開啟鬧鐘
					timer.schedule(new TimerTask() {// 設定鬧鐘時間
						@Override
						public void run() {
							litv();//爬蟲function
						}
					}, time * 1000, time * 1000);
					response.getWriter().println("setTime " + time + "s");
				} catch (Exception e) {
					response.getWriter().println("setTime error" + e.toString());
				}
			} else if(request.getParameter("resetCount") != null){
				count = 0;
				response.getWriter().println("resetCount:" + count);
			} else if(request.getParameter("resetError") != null){
				error.clear();
				response.getWriter().println("resetError:" + error.size());
			}else { 
				int day = time / (60 * 60 * 24);
				int hour = (time / (60 * 60)) % 24;
				int minute = (time / 60) % 60;
				int second = time % 60;
				response.getWriter().print(
						"戲劇爬蟲運行次數:" + count + 
						"</br>戲劇爬蟲運行週期:" + day + "天" + hour + "時" + minute + "分" + second + "秒"+
						"</br>戲劇爬蟲上一次運行時間:" + litvRunTime.get(0) + "年"+ litvRunTime.get(1) + "月"+ litvRunTime.get(2) + "日" + litvRunTime.get(3) + "時" + litvRunTime.get(4) + "分" + litvRunTime.get(5) + "秒"+
						"</br>錯誤次數:"+error.size());
				if(error.size()>0) {
					for(String message:error)
					response.getWriter().print("</br>錯誤訊息:"+message);
				}
			}
		}
	}
	private synchronized void litv(){
		count++;
		litvRunTime.clear();
		litvRunTime.add(Calendar.getInstance().get(Calendar.YEAR));
		litvRunTime.add(Calendar.getInstance().get(Calendar.MONTH)+1);
		litvRunTime.add(Calendar.getInstance().get(Calendar.DAY_OF_MONTH));
		litvRunTime.add(Calendar.getInstance().get(Calendar.HOUR_OF_DAY));
		litvRunTime.add(Calendar.getInstance().get(Calendar.MINUTE));
		litvRunTime.add(Calendar.getInstance().get(Calendar.SECOND));
		readDB DB = new readDB();
		try
		{
			
			// LiTV
			URL pageUrl = new URL("https://www.litv.tv/vod/ajax/getAllSimpleProgramByContentType?contentType=drama&_=1499321931414");
			
			BufferedReader reader = new BufferedReader(new InputStreamReader(pageUrl .openStream()));
			String URL;
			
			JSONArray temp = new JSONArray(reader.readLine());
			int lit = temp.length();
			lit = 50;

			for (int i = 0; i < lit; i++) 
			{ 
				URL = temp.getJSONObject(i).getString("landing_url");
				try 
				{
					Document doc = Jsoup.connect("https://www.litv.tv"+ URL)
										.timeout(10000)
										.get();
					String title = new String();
					String part = new String();
					String year = new String();
					String introduction = new String();
					String category = new String();
					String actor = new String();
					
					if (doc.select(".vod_title") != null)
						title = doc.select(".vod_title").text().toString();
					if (doc.select(".year .item_detail") != null)
						year = doc.select(".year .item_detail").text().toString();
					if (doc.select(".year .item_detail") != null)
						category = doc.select(".country .item_detail").text().toString();
					if (doc.select(".display_count") != null)
						part = doc.select(".display_count").text().toString();
					
					if (doc.select(".credits .item_detail_section").size() != 0)
						actor = doc.select(".credits .item_detail_section").get(0).text().toString();
					
					if (doc.select(".main_desc_section") != null)
						introduction = doc.select(".main_desc_section").text().toString();
					
					if ((!title.equals("")) && title != null)
					{
						//判斷重複
						ArrayList<HashMap<String, String>> data = DB.getDBresult("select * from " + tableName
								+ " where " + titleColumnName + "='" + title + "'");
						if (data.size() == 0) {// 沒有重複資料 寫入資料
							DB.writeDB("insert into " + tableName + "(" + 
									titleColumnName + "," + areaColumnName	+ "," + yearColumnName + ","+ partColumnName+ ","+ actorColumnName+ ","+ infoColumnName+") values('" + 
									title + "','" + category + "','" + year	+ "','" + part  + "','" + actor  + "','" + introduction+ "')");
		
						}
					}
				} 
				catch (Exception e) { error.add(e.toString()); }
			}
		} 
		catch (Exception e) 
		{ 
			error.add(e.toString());
		}
		DB.closeDB();
		DB = null;
	}
}
