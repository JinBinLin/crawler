package crawler;

import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.text.SimpleDateFormat;
import java.time.Year;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

@WebServlet(urlPatterns = { "/music" },loadOnStartup = 1)
public class music extends HttpServlet {
	private final String tableName = "st_music";// 資料庫名稱
	private final String singerColumnName = "singer";// 資料庫歌手欄位名稱
	private final String albumColumnName = "album";// 資料庫專輯欄位名稱
	private final String songColumnName = "song";// 資料庫歌手欄位名稱
	private int time = 60 * 60 * 24 * 7 ;// 設定週期(s)
	private Timer timer = new Timer();
	private ArrayList<String> error = new ArrayList<>();
	private int count = 0;
	private ArrayList<Integer> runTime = new ArrayList();
	
	public void init(ServletConfig config) throws ServletException {
		timer.schedule(new TimerTask() {
			@Override
			public void run() {
				mojim();//魔鏡爬蟲function
			}
		}, 0, time * 1000);
	}	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		if (request.getParameter("key") != null && request.getParameter("key").equalsIgnoreCase(setData.key)) {// 檢驗key
			if (request.getParameter("time") != null) {// 設定週期
				try {
					time = Integer.parseInt(request.getParameter("time"));
					timer.cancel();// 關掉鬧鐘
					timer = new Timer();// 開啟鬧鐘
					timer.schedule(new TimerTask() {// 設定鬧鐘時間
						@Override
						public void run() {
							mojim();//魔鏡爬蟲function
						}
					}, time * 1000, time * 1000);
					response.getWriter().println("setTime " + time + "s");
				} catch (Exception e) {
					response.getWriter().println("setTime error" + e.toString());
				}
			} else if(request.getParameter("resetCount") != null){
				count = 0;
				response.getWriter().println("resetCount:" + count);
			} else if(request.getParameter("resetError") != null){
				error.clear();
				response.getWriter().println("resetError:" + error.size());
			}else { 
				int day = time / (60 * 60 * 24);
				int hour = (time / (60 * 60)) % 24;
				int minute = (time / 60) % 60;
				int second = time % 60;
				response.getWriter().print(
						"音樂爬蟲運行次數:" + count + 
						"</br>音樂爬蟲運行週期:" + day + "天" + hour + "時" + minute + "分" + second + "秒"+
						"</br>音樂爬蟲上一次運行時間:" + runTime.get(0) + "年"+ runTime.get(1) + "月"+ runTime.get(2) + "日" + runTime.get(3) + "時" + runTime.get(4) + "分" + runTime.get(5) + "秒"+
						"</br>錯誤次數:"+error.size());
				if(error.size()>0) {
					for(String message:error)
					response.getWriter().print("</br>錯誤訊息:"+message);
				}
			}
		}
	}
	//魔鏡歌詞網
	public synchronized void mojim() {
		count++;
		readDB DB = new readDB();
		runTime.clear();
		runTime.add(Calendar.getInstance().get(Calendar.YEAR));
		runTime.add(Calendar.getInstance().get(Calendar.MONTH)+1);
		runTime.add(Calendar.getInstance().get(Calendar.DAY_OF_MONTH));
		runTime.add(Calendar.getInstance().get(Calendar.HOUR_OF_DAY));
		runTime.add(Calendar.getInstance().get(Calendar.MINUTE));
		runTime.add(Calendar.getInstance().get(Calendar.SECOND));
		try {
			for (int year = 2000; year <= Calendar.getInstance().get(Calendar.YEAR); year++) {// 從2000年的資料跑到今年
				for (int month = 1; month <= 12; month++) {// 跑12個月
					String musicURL = "http://mojim.com/twzlist" + year + "-" + String.format("%02d", month) + ".htm";

					Document doc = Jsoup.connect(musicURL).get();

					Elements newsHeadlines = doc.select("#inS dd");
					Elements temp = new Elements();
					Elements temp2 = new Elements();

					for (int i = 0; i < newsHeadlines.size(); i++) {
						temp = newsHeadlines.get(i).select("h1 .X2");
						temp2 = newsHeadlines.get(i).select(".t1");

						for (int j = 0; j < temp2.size(); j++) {
							String singer = "";
							String album = "";
							String song = "";
							for (int k = 0; k < temp.size(); k++) {
								if (temp.get(k).text().indexOf("'") != -1) {
									if (k == 0)
										singer = temp.get(k).text().replace("'", "＂");
									else
										album = temp.get(k).text().replace("'", "＂");
								} else {
									if (k == 0)
										singer = temp.get(k).text();
									else
										album = temp.get(k).text();
								}
							}

							if (temp2.get(j).text().indexOf("'") != -1) {
								song = temp2.get(j).text().replace("'", "＂");
							} else {
								song = temp2.get(j).text();
							}
							// 檢查資料是否重複 並且 寫入資料庫
//							System.out.println("select * from " + tableName
//									+ " where " + singerColumnName + "='" + singer + "' and " + albumColumnName + "='"
//									+ album + "' and " + songColumnName + "='" + song + "'");
							ArrayList<HashMap<String, String>> data = DB.getDBresult("select * from " + tableName
									+ " where " + singerColumnName + "='" + singer + "' and " + albumColumnName + "='"
									+ album + "' and " + songColumnName + "='" + song + "'");
//							System.out.println(data);
							if (data.size() == 0) {// 沒有重複資料
								DB.writeDB("insert into " + tableName + "(" + singerColumnName + "," + albumColumnName
										+ "," + songColumnName + ") values('" + singer + "','" + album + "','" + song
										+ "')");
							}
							data = null;
						}
					}

				}
			}
		} catch (Exception e) {
			error.add(e.toString());
		}
		DB.closeDB();
		DB = null;
	}
}
