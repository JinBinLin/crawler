  package crawler;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

/**
 * Servlet implementation class movie
 */
@WebServlet(urlPatterns= {"/movie"},loadOnStartup = 1)
public class movie extends HttpServlet {
	private String movieListTableName = "ts_movielist";// 資料表名稱
	private String[] movieListColumnName = new String[]{"即將上映","上映中","二輪新片"};
	private String movieTableName = "ts_movie";// 資料表名稱
	private String titleChColumnName = "title_ch";// 中文片名欄位名稱
	private String titleEngColumnName = "title_eng";// 英文片名欄位名稱
	private String categoryColumnName = "category";// 影片類型欄位名稱
	private String levelColumnName = "lv";// 影片分級欄位名稱
	private String languageColumnName = "lang";// 影片語系欄位名稱
	private String publicDateColumnName = "reDate";// 影片上映時間欄位名稱
	private String lengthColumnName = "leng";// 影片長度欄位名稱
	private String rateColumnName = "rate";// 影片評等欄位名稱
	private String infoColumnName = "info";// 影片資訊欄位名稱
	private String actorColumnName = "actor";// 影片演員欄位名稱
	private String directorColumnName = "director";// 影片導演欄位名稱
	private String scenaristColumnName = "scenarist";// 影片編劇欄位名稱
	private String countryColumnName = "country";// 影片發行國家欄位名稱
	private String companyColumnName = "company";// 影片發行公司欄位名稱
	private String introductionColumnName = "introduction";// 影片簡介欄位名稱
	private String IMDBColumnName = "IMDB_link";// 影片IMDB欄位名稱
	private String trailerColumnName = "trailer_url";// 影片預告片欄位名稱
	private String imgColumnName = "img_url";// 影片劇照欄位名稱
	private String posterColumnName = "poster_url";// 影片海報欄位名稱
	private String sourceColumnName = "mvsource";// 影片來源欄位名稱
	private int time = 60 * 60 * 24 * 7;// 設定週期(s)
	private Timer timer = new Timer();
	private ArrayList<String> error = new ArrayList<>();
	private int count = 0;
	private ArrayList<Integer> runTime = new ArrayList();
 
	public void init(ServletConfig config) throws ServletException {
		timer.schedule(new TimerTask() {
			@Override
			public void run() {
				atMovie();//開眼電影網爬蟲function
			} 
		}, 0, time * 1000);
	}	
	
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		if (request.getParameter("key") != null && request.getParameter("key").equalsIgnoreCase(setData.key)) {// 檢驗key
			if (request.getParameter("time") != null) {// 設定週期
				try {
					time = Integer.parseInt(request.getParameter("time"));
					timer.cancel();// 關掉鬧鐘
					timer = new Timer();// 開啟鬧鐘
					timer.schedule(new TimerTask() {// 設定鬧鐘時間
						@Override
						public void run() {
							atMovie();//開眼電影網爬蟲function
						}
					}, time * 1000, time * 1000);
					response.getWriter().println("setTime " + time + "s");
				} catch (Exception e) {
					response.getWriter().println("setTime error" + e.toString());
				}
			} else if(request.getParameter("resetCount") != null){
				count = 0;
				response.getWriter().println("resetCount:" + count);
			} else if(request.getParameter("resetError") != null){
				error.clear();
				response.getWriter().println("resetError:" + error.size());
			}else { 
				int day = time / (60 * 60 * 24);
				int hour = (time / (60 * 60)) % 24;
				int minute = (time / 60) % 60;
				int second = time % 60;
				response.getWriter().print(
						"開眼爬蟲運行次數:" + count + 
						"</br>音樂爬蟲運行週期:" + day + "天" + hour + "時" + minute + "分" + second + "秒"+
						"</br>音樂爬蟲上一次運行時間:" + runTime.get(0) + "年"+ runTime.get(1) + "月"+ runTime.get(2) + "日" + runTime.get(3) + "時" + runTime.get(4) + "分" + runTime.get(5) + "秒"+
						"</br>錯誤次數:"+error.size());
				if(error.size()>0) {
					for(String message:error)
					response.getWriter().print("</br>錯誤訊息:"+message);
				}
			}
		}
	}
 
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}
	private void atMovie() {
		count++;
		runTime.clear();
		runTime.add(Calendar.getInstance().get(Calendar.YEAR));
		runTime.add(Calendar.getInstance().get(Calendar.MONTH)+1);
		runTime.add(Calendar.getInstance().get(Calendar.DAY_OF_MONTH));
		runTime.add(Calendar.getInstance().get(Calendar.HOUR_OF_DAY));
		runTime.add(Calendar.getInstance().get(Calendar.MINUTE));
		runTime.add(Calendar.getInstance().get(Calendar.SECOND));
		readDB DB = new readDB();
		String[] state = new String[]{"next","now","now2"};

		for (int i = 0; i < state.length; i++) {
			Elements li = new Elements();
			String url = "http://www.atmovies.com.tw/movie/" + state[i] + "/0/"; // 近期上映
			Document doc;
			try {
				doc = Jsoup.connect(url).get();

			if (state[i].startsWith("now"))
				 li = doc.select(".filmListAll li a"); // 近期上映
			if (state[i].startsWith("next"))
				 li = doc.select(".filmNextListAll li a"); // 上映中
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			String mvList = li.text().replace(" ", "|");

			mvList = mvList.substring(1).replaceAll("－", "-").replaceAll("–", "-").replaceAll("‘", "'")
					.replaceAll("’", "'").replaceAll("（", "\\(")
					.replaceAll("）", "\\)").replaceAll("＆", "&")
					.replaceAll("…", "...").replaceAll("”", "'").replaceAll("“", "'") 
					.replaceAll("？", "?").replaceAll("：", ":").replaceAll("︰", ":");
			//更新list
			DB.writeDB("update " + movieListTableName + " set mvList= '" + mvList + "' where mvType = '" + movieListColumnName[i] + "'");
			if (i != 2)
			{
				for (int j = 0; j < li.size(); j++) {
					if (li.get(j).hasAttr("href")) {
						if (!li.get(j).attr("href").split("/")[2].startsWith("f")) { continue; }
						String id = li.get(j).attr("href").split("/")[2];
						String title_list = li.get(j).text();
						//新增data
						try {
							
							doc = Jsoup.connect("http://mobileweb.atmovies.com.tw:8080/movie/" + id + "/now/1234567890123456/").get();
							
							String title = doc.select(".filmTitle").text();
							
							boolean f = false;
							
							String level = new String();
							if (doc.select(".showdate img").attr("src").startsWith("/images/"))
							{
								switch (doc.select(".showdate img").attr("src").split("/")[2].toLowerCase()) {
								case "cer_r.gif":
									level = "限制級";
									break;
								case "cer_p.gif":
									level = "保護級";
									break;
								case "cer_pg.gif":
									level = "保護級";
									break;
								case "cer_f5.gif":
									level = "輔導級";
									break;
								case "cer_f2.gif":
									level = "輔導級";
									break;
								case "cer_g.gif":
									level = "普遍級";
									break;
									
								default:
									break;
								}
							}
							
							String rate = doc.select(".thisRating").text();
							
							String trailer_url = doc.select(".trailer iframe").attr("src");
							String info = doc.select(".tagline").text();
							
							String intro = new String();
							
							int introi = doc.select("div").size() - 2;
							
							intro = doc.select("div").get(introi).text();
							
							String img_url = new String();
							//取得圖片
							Elements img = Jsoup.connect("http://app2.atmovies.com.tw/photo/" + id + "/").get().select(".showphoto span a");
							if (img.size() > 0)
							{ img_url = img.get(0).attr("href"); }
							
							String poster_url = new String();
							Elements poster = Jsoup.connect("http://app2.atmovies.com.tw/poster/" + id + "/").get().select(".showphoto span a");
							for(int z = 0; z < poster.size(); z++)
							{
								if (z == 0)
								{ poster_url = poster.get(z).attr("href"); }
								
								String[] temp = poster.get(z).attr("href").split("/");
								if (temp[temp.length - 1].startsWith("px"))
								{
									poster_url = poster.get(z).attr("href");
									break;
								}
							}
							//抓取導演、編劇、演員
							Elements casts = Jsoup.connect("http://app2.atmovies.com.tw/film/cast/" + id + "/").get().select("#main-wrapper ul");
							
							List<String> dir = new ArrayList<>();
							List<String> scen = new ArrayList<>();
							List<String> actor = new ArrayList<>(); 
							int castIndex = -1;
							f = false;
							for (int index = 0; index < casts.select("li").size(); index++) {
								if (casts.select("li").get(index).childNodeSize() == 1) {
									switch (casts.select("li").get(i).text()) {
									case "導演：":
										castIndex = 0;
										f = true;
										break;
									case "編劇：":
										castIndex = 1;
										f = true;
										break;
									case "演員：":
										castIndex = 2;
										f = true;
										break;
										
									default:
										f = false;
										break;
									}
								}
								else
								{
									if(f)
									{
										switch(castIndex){
										case 0:
//											System.out.println("0: "+casts.select("li").get(index).select("a").text());
											
											dir.add(casts.select("li").get(index).select("a").text());
											break;
										case 1:

//											System.out.println("1: "+casts.select("li").get(index).select("a").text());
											scen.add(casts.select("li").get(index).select("a").text());
											break;
										case 2:

//											System.out.println("2: "+casts.select("li").get(index).select("a").text());
											actor.add(casts.select("li").get(index).select("a").text());
											break;
										default:
											break;
										}
									}
								}
							}
							
							Document web = Jsoup.connect("http://www.atmovies.com.tw/movie/"+ id).get();
							Elements detail = web.select("#filmCastDataBlock ul").get(1).select("li");
							
							
							String IMDB_link = new String();
							String country = new String();
							String lang = new String();
							String len = new String();
							String date_release = new String();
							List<String> company = new ArrayList<>();
							
							for (int x = 0; x < detail.size(); x++) {
								if (detail.get(x).text().equals("IMDb")) { IMDB_link = detail.get(x).select("a").attr("href"); }
								if (detail.get(x).text().indexOf("出  品  國：") > -1 && detail.get(x).text().length() > 8) { country = detail.get(x).text().split("：")[1]; }
								if (detail.get(x).text().indexOf("出　　品：") > -1 && detail.get(x).text().length() > 5) { company.add(detail.get(x).text().split("：")[1]); }
								if (detail.get(x).text().indexOf("發  行  商：") > -1 && detail.get(x).text().length() > 8) { company.add(detail.get(x).text().split("：")[1]); }
								if (detail.get(x).text().indexOf("語　　言：") > -1 && detail.get(x).text().length() > 5) { lang = detail.get(x).text().split("：")[1]; }
								if (detail.get(x).text().indexOf("影片年份：") > -1 && detail.get(x).text().length() > 5) { date_release = detail.get(x).text().split("：")[1]; }
							}
							
							
							detail = web.select("#filmTagBlock .runtime li");
							for (int x = 0; x < detail.size(); x++) {
								if (detail.get(x).text().startsWith("上映日期：")) { date_release = detail.get(x).text().split("：")[1]; }
								if (detail.get(x).text().startsWith("片長：")) { len = detail.get(x).text().split("：")[1]; }
							}
							
							String actors = new String();
							for (int x = 0; x < actor.size(); x++) {
								actors += actor.get(x);
								if (x != (actor.size() - 1)) { actors += "|"; }
							}
							
							String dirs = new String();
							for (int x = 0; x < dir.size(); x++) {
								dirs += dir.get(x);
								if (x != (dir.size() - 1)) { dirs += "|"; }
							}
							
							String scens = new String();
							for (int x = 0; x < scen.size(); x++) {
								scens += scen.get(x);
								if (x != (scen.size() - 1)) { scens += "|"; }
							}
							
							String companys = new String();
							for (int x = 0; x < company.size(); x++) {
								companys += company.get(x);
								if (x != (company.size() - 1)) { companys += "|"; }
							}
							
							
							title = title.replaceAll("－", "-").replaceAll("–", "-").replaceAll("‘", "'")
									.replaceAll("’", "'").replaceAll("（", "\\(")
									.replaceAll("）", "\\)").replaceAll("＆", "&")
									.replaceAll("…", "...").replaceAll("”", "'").replaceAll("“", "'")
									.replaceAll("？", "?").replaceAll("：", ":").replaceAll("︰", ":");
							
							title = title.replaceAll("\\(\\d+\\)", "").replaceAll(" 3D", "").replaceAll("&", "and");
							String[] temp = title.split("\\s");
							String title_ch = "";
							String title_eng = "";
							int index = temp.length - 1;
							for (int z = temp.length - 1; z >= 0; z--) {
								int g = 0;
								for (int x = 0; x < temp[z].length(); x++) {
									if ((int) temp[z].charAt(x) > 32 && (int) temp[z].charAt(x) < 126 )
									{
										g++;
									}
								}
								if (g == temp[z].length())
								{ index = z; }
								else
								{ break; }
							}
							
							for (int z = 0; z < temp.length; z++) {
								if (z < index) {
									title_ch += " " + temp[z];
								}
								else {
									title_eng += " " + temp[z];
								}
							}
							
							title_eng = title_eng.trim();
							title_ch = title_ch.trim();
							
							if (title_ch.equals("") || (!title.equals(title_list) && !title_list.equals("")))
							{ title_ch = title_list; }
							
							title_ch = title_ch.replaceAll("－", "-").replaceAll("–", "-").replaceAll("‘", "'")
									.replaceAll("’", "'").replaceAll("（", "\\(")
									.replaceAll("）", "\\)").replaceAll("＆", "&")
									.replaceAll("…", "...").replaceAll("”", "'").replaceAll("“", "'")
									.replaceAll("？", "?").replaceAll("：", ":").replaceAll("︰", ":");
							
							System.out.println(title_ch.replaceAll("'", "’") + " : " + title_eng.replaceAll("'", "’") + " : " + level);
							
							//寫入資料庫
							ArrayList<HashMap<String, String>> data = DB.getDBresult("select * from "
										+ movieTableName + " where title_ch = '" + title_ch.replaceAll("'", "’")+ "'");
							System.out.print("select * from "
									+ movieTableName + " where title_ch = '" + title_ch.replaceAll("'", "’")+ "'");
							if(data.size() > 0) {
								
							}else {
								System.out.println("insert into "+movieTableName +"("
										+titleChColumnName+","
										+titleEngColumnName+","
										+levelColumnName+","
										+languageColumnName+","
										+publicDateColumnName+","
										+lengthColumnName+","
										+rateColumnName+","
										+infoColumnName+","
										+actorColumnName+","
										+directorColumnName+","
										+scenaristColumnName+","
										+countryColumnName+","
										+companyColumnName+","
										+introductionColumnName+","
										+IMDBColumnName+","
										+trailerColumnName+","
										+imgColumnName+","
										+posterColumnName+","
										+sourceColumnName
										+") values ('"
										+title_ch.replaceAll("'", "’")+"','"
										+title_eng.replaceAll("'", "’")+"','"
										+level+"','"
										+lang+"','"
										+date_release+"','"
										+len+"','"
										+rate+"','"
										+info.replaceAll("'", "’")+"','"
										+actors.replaceAll("'", "’")+"','"
										+dirs.replaceAll("'", "’")+"','"
										+scens.replaceAll("'", "’")+"','"
										+country+"','"
										+companys.replaceAll("'", "’") +"','"
										+intro.replaceAll("'", "’").substring(5)+"','"
										+IMDB_link+"','"
										+trailer_url+"','"
										+img_url+"','"
										+poster_url+"','"
										+"開眼電影網"+"')");
								DB.writeDB("insert into "+movieTableName +"("
										+titleChColumnName+","
										+titleEngColumnName+","
										+categoryColumnName+","
										+levelColumnName+","
										+languageColumnName+","
										+publicDateColumnName+","
										+lengthColumnName+","
										+rateColumnName+","
										+infoColumnName+","
										+actorColumnName+","
										+directorColumnName+","
										+scenaristColumnName+","
										+countryColumnName+","
										+companyColumnName+","
										+introductionColumnName+","
										+IMDBColumnName+","
										+trailerColumnName+","
										+imgColumnName+","
										+posterColumnName+","
										+sourceColumnName
										+") values ('"
										+title_ch.replaceAll("'", "’")+"','"
										+title_eng.replaceAll("'", "’")+"','"
										+""+"','"
										+level+"','"
										+lang+"','"
										+date_release+"','"
										+len+"','"
										+rate+"','"
										+info.replaceAll("'", "’")+"','"
										+actors.replaceAll("'", "’")+"','"
										+dirs.replaceAll("'", "’")+"','"
										+scens.replaceAll("'", "’")+"','"
										+country+"','"
										+companys.replaceAll("'", "’") +"','"
										+intro.replaceAll("'", "’").substring(5)+"','"
										+IMDB_link+"','"
										+trailer_url+"','"
										+img_url+"','"
										+poster_url+"','"
										+"開眼電影網"+"')"
										);
							}
							
						}
						catch (Exception e) { 
							error.add(e.toString());
							continue;}
						
					}
				}
			}
		}
	} 
}
