package crawler;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;

public class readDB {
	private Connection conn = null;
	private Statement stmt = null;
	private ResultSet result = null;
	private final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
	private String url = "jdbc:mysql://localhost:3306/rec?useUnicode=true&characterEncoding=UTF-8&zeroDateTimeBehavior=convertToNull";
	private final String user = "jb";
	private final String password = "jb00000000";
	private final int connectMaxIndex = 3;// 沽刚硈絬程Ω计
	private int connectIndex = 0;// 沽刚硈絬Ω计
	private int closeIndex = 0;// 沽刚闽超硈絬Ω计
	private final int  threadSleepTime = 100;//睝
	public readDB() {
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		connectDB();
	}
	public readDB(String DBname) {
		url = "jdbc:mysql://localhost:3306/"+DBname+"?useUnicode=true&characterEncoding=UTF-8&zeroDateTimeBehavior=convertToNull";
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		connectDB();
	}
	private void connectDB(){
		try {
			conn = DriverManager.getConnection(url, user, password);
			stmt = conn.createStatement();
		} catch (SQLException e) {
			System.out.println(e.toString());
			try {
				Thread.sleep(threadSleepTime);
				if (connectIndex <= connectMaxIndex){ // 程计
					connectIndex++;
					connectDB();// 穝禲
				}
			} catch (InterruptedException ex) {
				Thread.currentThread().interrupt();
				System.out.println(ex.toString());
			}
		}
	}
	

	// 肚SQL琩高挡狦
	public ArrayList<HashMap<String, String>> getDBresult(String sql) {
		ArrayList<HashMap<String, String>> data = new ArrayList<>();
		if(sql == null || stmt == null || sql.equalsIgnoreCase(""))
			return data;
		try {
			result = stmt.executeQuery(sql);
			while (result.next()) {
				HashMap<String, String> item = new HashMap<>();
				for (int index = 1; index <= result.getMetaData().getColumnCount(); index++) {
					item.put(result.getMetaData().getColumnName(index), result.getString(index));
				}
				data.add(item);
			}
			connectIndex = 0;// 琩高Ω计耴箂
		} catch (SQLException e) {
			System.out.println(e.toString());
			try {
				Thread.sleep(threadSleepTime);
				if (connectIndex <= connectMaxIndex){ // 程计
					connectIndex++;
					getDBresult(sql);// 穝禲
				}
			} catch (InterruptedException ex) {
				Thread.currentThread().interrupt();
				System.out.println(ex.toString());
			}
		}
		return data;
	}

	public void writeDB(String sql) {
//		
		try {
			stmt.executeUpdate(sql);
			connectIndex = 0;// 琩高Ω计耴箂
		} catch (SQLException e) {
			System.out.println(e.toString());
			try {
				Thread.sleep(threadSleepTime);
				if (connectIndex <= connectMaxIndex){ // 程计
					connectIndex++;
					writeDB(sql);// 穝禲
				}
			} catch (InterruptedException ex) {
				Thread.currentThread().interrupt();
				System.out.println(ex.toString());
			}
		}
	}

	public void closeDB() {
		try {
			if(result != null)
			result.close();
			if(stmt != null)
			stmt.close();
			if(conn != null)
			conn.close();
			result = null;
			stmt = null;
			conn = null;
		} catch (SQLException e) {
			System.out.println(e.toString());
			try {
				Thread.sleep(threadSleepTime);
				if (closeIndex <= connectMaxIndex){ // 程计
					closeDB();// 穝禲
					closeIndex++;
				}
			} catch (InterruptedException ex) {
				Thread.currentThread().interrupt();
				System.out.println(ex.toString());
			}
		}
	}
}
