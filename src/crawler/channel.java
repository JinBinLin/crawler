package crawler;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sound.midi.Synthesizer;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

@WebServlet(urlPatterns = "/channel" ,loadOnStartup = 1)
public class channel extends HttpServlet{
	private final String tableName = "st_tv";// 資料表名稱
	private final String channelColumnName = "channelName";// 頻道名稱欄位名稱
	private final String channelNumberColumnName = "channelNo";// 頻道台數欄位名稱
	private final String recNameColumnName = "vdName";// 語音專用欄位名稱
	private final String recNumberColumnName = "vdName1";// 語音專用台數欄位名稱
	private final String channelIconColumnName = "tvImage";// 頻道圖片欄位名稱
	private int time =  60 * 60 * 24 * 7;// 設定週期(s)
	private Timer timer = new Timer();
	private ArrayList<String> error = new ArrayList<>();
	private int count = 0;
	private ArrayList<Integer> litvRunTime = new ArrayList();
	
	public void init(ServletConfig config) throws ServletException {
		timer.schedule(new TimerTask() {
			@Override
			public void run() {
				litv();//爬蟲function
			}
		}, 0, time * 1000);
	}	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		if (request.getParameter("key") != null && request.getParameter("key").equalsIgnoreCase(setData.key)) {// 檢驗key
			if (request.getParameter("time") != null) {// 設定週期
				try {
					time = Integer.parseInt(request.getParameter("time"));
					timer.cancel();// 關掉鬧鐘
					timer = new Timer();// 開啟鬧鐘
					timer.schedule(new TimerTask() {// 設定鬧鐘時間
						@Override
						public void run() {
							litv();//爬蟲function
						}
					}, time * 1000, time * 1000);
					response.getWriter().println("setTime " + time + "s");
				} catch (Exception e) {
					response.getWriter().println("setTime error" + e.toString());
				}
			} else if(request.getParameter("resetCount") != null){
				count = 0;
				response.getWriter().println("resetCount:" + count);
			} else if(request.getParameter("resetError") != null){
				error.clear();
				response.getWriter().println("resetError:" + error.size());
			}else { 
				int day = time / (60 * 60 * 24);
				int hour = (time / (60 * 60)) % 24;
				int minute = (time / 60) % 60;
				int second = time % 60;
				response.getWriter().print(
						"頻道爬蟲運行次數:" + count + 
						"</br>頻道爬蟲運行週期:" + day + "天" + hour + "時" + minute + "分" + second + "秒"+
						"</br>頻道爬蟲上一次運行時間:" + litvRunTime.get(0) + "年"+ litvRunTime.get(1) + "月"+ litvRunTime.get(2) + "日" + litvRunTime.get(3) + "時" + litvRunTime.get(4) + "分" + litvRunTime.get(5) + "秒"+
						"</br>錯誤次數:"+error.size());
				if(error.size()>0) {
					for(String message:error)
					response.getWriter().print("</br>錯誤訊息:"+message);
				}
			}
		}
	}
	private synchronized void litv() {
		count++;
		litvRunTime.clear();
		litvRunTime.add(Calendar.getInstance().get(Calendar.YEAR));
		litvRunTime.add(Calendar.getInstance().get(Calendar.MONTH)+1);
		litvRunTime.add(Calendar.getInstance().get(Calendar.DAY_OF_MONTH));
		litvRunTime.add(Calendar.getInstance().get(Calendar.HOUR_OF_DAY));
		litvRunTime.add(Calendar.getInstance().get(Calendar.MINUTE));
		litvRunTime.add(Calendar.getInstance().get(Calendar.SECOND));
		readDB DB = new readDB();
		try {
			Document doc = Jsoup.connect("https://www.litv.tv/channel/list.do#ALL")
					.timeout(10000)
					.get();
			Elements temp = doc.select(".channel_li_table_counter");
			
			for (int i = 0; i < temp.size(); i++) {
				Elements temp2 = temp.get(i).select("tr");
				for (int j = 1; j < temp2.size(); j++) {
					String title = temp2.get(j).select(".channels_title").text();
					String No = temp2.get(j).select("td").get(0).text();
					//判斷資料重複
					ArrayList<HashMap<String, String>> data = DB.getDBresult(
							"select * from " + tableName+ " where "+channelNumberColumnName+" = " + No + ";");
					if (data.size() == 0) {// 沒有重複資料 寫入資料
						DB.writeDB("insert into " + tableName + "(" + 
								channelColumnName + "," + channelNumberColumnName	+ "," + recNameColumnName + ","+ recNumberColumnName+ ","+ channelIconColumnName+ ") values('" + 
								title + "'," + No + ",'" + title.replace("台","")	+ "','" + Integer.parseInt(No) + "台','" + temp2.get(j).select("img").attr("src")+"')");
					}else {
						if(data.get(0).get(channelColumnName).equalsIgnoreCase(title)) {
							
						}else {
							DB.writeDB("UPDATE " + tableName + " SET " + 
									channelColumnName +" ='"+title + "'," + channelNumberColumnName	+" ="+No + "," + recNameColumnName +" ='"+title.replace("台","") + "',"
									+ recNumberColumnName+" ='"+Integer.parseInt(No) + "台',"+ channelIconColumnName+" ='"+temp2.get(j).select("img").attr("src") + "' where "+channelNumberColumnName+" = " + No );

						}
					}
				}
			}
		} 
		catch(Exception e) 
		{ 
			error.add(e.toString());
		}
		DB.closeDB();
		DB = null;
	}
}
